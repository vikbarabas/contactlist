package hu.vikbarabas.contantgroups.repository;

import hu.vikbarabas.contantgroups.entity.ContactGroups;
import java.util.List;

public interface IContactGroups {

    long add(ContactGroups contactGroups);

    List<ContactGroups> findAll();
}
