package hu.vikbarabas.contantgroups.repository;

import hu.vikbarabas.contantgroups.entity.ContactGroups;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class ContactGroupsRepository implements IContactGroups {

    @Autowired
    EntityManager entityManager;

    @Override
    public long add(ContactGroups contactGroups) {
        entityManager.persist(contactGroups);
        return contactGroups.getId();
    }

    @Override
    public List<ContactGroups> findAll() {
        List<ContactGroups> contactGroups = entityManager
                .createQuery("select cg from ContactGroups cg", ContactGroups.class)
                .getResultList();

        return contactGroups;
    }
}
