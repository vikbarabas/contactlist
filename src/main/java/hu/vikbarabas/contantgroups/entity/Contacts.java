package hu.vikbarabas.contantgroups.entity;

import javax.persistence.*;

@Entity
public class Contacts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Contacts() {
    }

    private String firstName;

    private String lastName;

    public Contacts(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
