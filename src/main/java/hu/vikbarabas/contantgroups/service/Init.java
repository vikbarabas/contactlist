package hu.vikbarabas.contantgroups.service;

import hu.vikbarabas.contantgroups.entity.ContactGroups;
import hu.vikbarabas.contantgroups.entity.Contacts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class Init implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private Init self;

    private boolean initialization = false;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void populateData() {
        Contacts contacts = new Contacts("Próba", "János");
        entityManager.persist(contacts);

        entityManager.persist(new ContactGroups("group1", "First Group"));
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (!initialization) {
            self.populateData();

            initialization = true;
        }
    }
}
