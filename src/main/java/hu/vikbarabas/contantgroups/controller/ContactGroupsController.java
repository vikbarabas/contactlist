package hu.vikbarabas.contantgroups.controller;

import hu.vikbarabas.contantgroups.dto.ContactGroupsJSON;
import hu.vikbarabas.contantgroups.dto.ContactGroupsJSONAssembler;
import hu.vikbarabas.contantgroups.entity.ContactGroups;
import hu.vikbarabas.contantgroups.repository.IContactGroups;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@ExposesResourceFor(ContactGroups.class)
@RequestMapping(path = "/contactgroups", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, "application/hal+json"})
public class ContactGroupsController {

    private IContactGroups contactGroupsRepository;
    private ContactGroupsJSONAssembler jsonAssembler;
    private EntityLinks entityLinks;

    public ContactGroupsController(IContactGroups contactGroupsRepository, ContactGroupsJSONAssembler jsonAssembler, EntityLinks entityLinks) {
        this.contactGroupsRepository = contactGroupsRepository;
        this.jsonAssembler = jsonAssembler;
        this.entityLinks = entityLinks;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity newGroup(@RequestBody ContactGroups newGroup) {
        long contactGroupsId = contactGroupsRepository.add(newGroup);

        URI createdURI = entityLinks.linkForSingleResource(ContactGroups.class, contactGroupsId).toUri();

        return ResponseEntity
                .created(createdURI)
                .build();
    }

    @GetMapping
    public Resources<ContactGroupsJSON> findAll() {
        Resources<ContactGroupsJSON> all = new Resources<>(jsonAssembler.toResources(contactGroupsRepository.findAll()));
        all.add(entityLinks.linkToCollectionResource(ContactGroups.class).withSelfRel());

        return all;
    }
}
