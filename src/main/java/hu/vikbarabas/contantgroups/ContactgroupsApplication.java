package hu.vikbarabas.contantgroups;

import hu.vikbarabas.contantgroups.dto.ContactGroupsJSONAssembler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.config.EnableEntityLinks;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@SpringBootApplication
public class ContactgroupsApplication {

    @PersistenceContext
    EntityManager entityManager;

    @Bean
    ContactGroupsJSONAssembler bookJSONAssembler(EntityLinks entityLinks) {
        return new ContactGroupsJSONAssembler(entityLinks);
    }

    public static void main(String[] args) {
        SpringApplication.run(ContactgroupsApplication.class, args);
    }

}
