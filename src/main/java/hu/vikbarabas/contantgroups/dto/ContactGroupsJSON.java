package hu.vikbarabas.contantgroups.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import hu.vikbarabas.contantgroups.entity.ContactGroups;
import org.springframework.hateoas.ResourceSupport;

@JsonPropertyOrder({"id", "name", "displayName"})
public class ContactGroupsJSON extends ResourceSupport {
    ContactGroups contactGroups;

    public ContactGroupsJSON(ContactGroups contactGroups) {
        this.contactGroups = contactGroups;
    }

    public String getName() {
        return contactGroups.getName();
    }

    public String getDisplayName() {
        return contactGroups.getDisplayName();
    }
}
