package hu.vikbarabas.contantgroups.dto;

import hu.vikbarabas.contantgroups.controller.ContactGroupsController;
import hu.vikbarabas.contantgroups.entity.ContactGroups;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

public class ContactGroupsJSONAssembler extends ResourceAssemblerSupport<ContactGroups, ContactGroupsJSON> {
    private EntityLinks entityLinks;

    public ContactGroupsJSONAssembler(EntityLinks entityLinks) {
        super(ContactGroupsController.class, ContactGroupsJSON.class);
        this.entityLinks = entityLinks;
    }

    @Override
    public ContactGroupsJSON toResource(ContactGroups contactGroups) {
        ContactGroupsJSON contactGroupsJSON = new ContactGroupsJSON(contactGroups);

        contactGroupsJSON.add(entityLinks
        .linkForSingleResource(ContactGroups.class, contactGroups.getId()).withSelfRel());

        return contactGroupsJSON;
    }
}
